Projeto desenvolvido como trabalho final do curso intermediário de Unity C# - Studica ministrado na Tapps Games

Aluno: Thiago Nagaoka
Instrutor: Hugo e Danilo

- O trabalho contém o executável e projeto desenvolvido.
- Foi produzido em Mac, utilizando a versão 2017.4.15.f1 da Unity.
- Utilizei como base modelos gráficos baixados na asset store de um demo tutorial da propria Unity (Tanks) e de outros projetos free (Zombie pack).
- O jogo foi desenvolvido para jogar multiplayer, de 2 a 4 pessoas.
- O jogo consiste em um multiplayer cooperativo, onde cada jogador controla um tank com objetivo de eliminar todos os zumbis do cenário. O jogo acaba quando todos os zumbis estiverem mortos, ou todos os jogadores estiverem mortos. 2 Spawners fazem zumbis aparecerem no cenário a cada 15 segundos, aumentando a dificuldade. Os tiros de canhão dos tanques podem ferir os outros tanques, como também o próprio tanque que o lançou.
Sobre os tópicos aprendidos em treinamento:
- Foi utilizado a High Level API para a parte de networking multiplayer
- Os zumbis utilizam-se de animações configurados por meio do Animator, triggers e variáveis.
- Foi implementando um sistema de pontuação para armazenar localmente os pontos e Ranking.
