﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

	void Start(){
		transform.LookAt(Camera.main.transform);
	}

	//Set gameObject to face to camera
	void Update(){
		var targetRotation = Quaternion.LookRotation(Camera.main.transform.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 8f * Time.deltaTime);
	}
}
