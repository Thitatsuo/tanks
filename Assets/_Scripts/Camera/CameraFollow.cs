﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform cameraTankPosition; 
	public float smoothing = 1f;

	void Awake(){
		SetCameraToDefaultPosition();
		EventManager.onLocalPlayerStarted += SetCameraToTankPosition;
	}

	public void SetCameraToDefaultPosition(){
		transform.position = new Vector3(0f, 70f, -15f);
		transform.eulerAngles = new Vector3(80f, 0f, 0f);
	}

	//Set camera to Tank Camera Position to 3th person view
	public void	SetCameraToTankPosition(GameObject playerTank){
		cameraTankPosition = playerTank.GetComponent<SimpleTankController>().GetCameraPositionTransform();
	}

	//Positioning camera on update to follow tank
	void LateUpdate(){
		if (cameraTankPosition) {
			transform.position = Vector3.Slerp(transform.position, cameraTankPosition.position, smoothing);
			transform.rotation = Quaternion.Slerp(transform.rotation, cameraTankPosition.rotation, smoothing * 1.5f);
		}
	}
}
