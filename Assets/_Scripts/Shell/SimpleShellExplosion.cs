﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SimpleShellExplosion : NetworkBehaviour {
	public ParticleSystem m_ExplosionParticles;      // Reference to the explosions particlesn.
	public float maxDamage;                          // Max damage done in center of explosion.
	public float explosionForce;                     // The amount of force added to a tank at the centre of the explosion.
	public float maxLifeTime;                        // The time in seconds before the shell is removed.
	public float explosionRadius;                    // The maximum distance away from the explosion tanks can be and are still affected.

	public AudioSource m_ExplosionAudio;             // Reference to the audio that will play on explosion.

	private int playersLayer;                       // A layer mask so that only the tanks are affected by the explosion.
	private int enemiesLayer;                       // A layer mask so that only the tanks are affected by the explosion.
	private string owner;                           // The player owner of attack.

	private void Awake(){
		SetConfigs();
	}

	public void SetConfigs(){
		maxDamage = ShellConfigs.simpleShellConfigs["maxDamage"];
		explosionForce = ShellConfigs.simpleShellConfigs["explosionForce"];
		maxLifeTime = ShellConfigs.simpleShellConfigs["maxLifeTime"];   
		explosionRadius = ShellConfigs.simpleShellConfigs["explosionRadius"];
	}

	public void setOwner(string attacker){
		owner = attacker;
	}

	private void Start(){
		GetComponent<Collider>().enabled = false;
		StartCoroutine(EnableCollision());

		playersLayer = LayerMask.GetMask("Players");
		enemiesLayer = LayerMask.GetMask("Enemies");
	}

	//allow to delay a bit the activation of the collider so that it don't collide when spawn close to the canon
	IEnumerator EnableCollision(){
		yield return new WaitForSeconds(0.1f);
		GetComponent<Collider>().enabled = true;
	}

	// Calculate explosion damage based on distance
	private float CalculateDamage(Transform damagedTransform){
		Vector3 explosionToTarget = damagedTransform.position - transform.position;
		float explosionDistance = explosionToTarget.magnitude;
		float relativeDistance = (explosionRadius - explosionDistance) / explosionRadius;
		return Mathf.Max(0f, relativeDistance * maxDamage);
	}

	//Check if collided if any Player
	private void CheckTankDamage(){
		Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, playersLayer);

		for(int i = 0; i < colliders.Length; i++)
		{
			Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
			if(!targetRigidbody)
				continue;

			TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();
			if(!targetHealth)
				continue;

			targetHealth.Damage(CalculateDamage(targetRigidbody.transform));
		}

	}

	//Check if collided if any Enemy
	private void CheckZombiesDamage(){
		Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, enemiesLayer);

		for(int i = 0; i < colliders.Length; i++)
		{
			Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
			if(!targetRigidbody)
				continue;

			SimpleZombieHealth targetHealth = targetRigidbody.GetComponent<SimpleZombieHealth>();
			if(!targetHealth)
				continue;

			targetHealth.Damage(CalculateDamage(targetRigidbody.transform), owner);
		}

	}
		
	[ServerCallback]
	private void OnTriggerEnter(Collider other){
		CheckTankDamage();
		CheckZombiesDamage();

		if (!NetworkClient.active)//if we are ALSO client (so hosting), this will be done by the Destroy so Skip
			PhysicForces();

		// Destroy the shell on clients.
		NetworkServer.Destroy(gameObject);
	}

	//called on client when the Network destroy that object (it was destroyed on server)
	public override void OnNetworkDestroy(){
		ExplodeShell();

		//set the particle to be destroyed at the end of their lifetime
		ParticleSystem.MainModule mainModule = m_ExplosionParticles.main;
		Destroy(m_ExplosionParticles.gameObject, mainModule.duration);
		base.OnNetworkDestroy();
	}

	void ExplodeShell(){
		// Unparent the particles from the shell.
		m_ExplosionParticles.transform.parent = null;


		m_ExplosionParticles.Play();
		m_ExplosionAudio.Play();

		PhysicForces();
	}

	//This apply force on object. Do that on all clients & server as each must apply force to object they own
	void PhysicForces(){
		Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, playersLayer);

		for (int i = 0; i < colliders.Length; i++)
		{
			Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
			if (!targetRigidbody)
				continue;

			targetRigidbody.AddExplosionForce(explosionForce, transform.position, explosionRadius);
		}
	}
}
