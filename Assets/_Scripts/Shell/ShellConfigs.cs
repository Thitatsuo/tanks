﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class ShellConfigs {
	public static Dictionary<string, float> simpleShellConfigs = new Dictionary<string, float>()
	{
		{ "maxDamage", 100f },
		{ "explosionForce", 500f },
		{ "maxLifeTime", 2f },
		{ "explosionRadius", 5f }
	};
}