﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class SimpleZombieConfigs {

	//moving Default Configs
	public float zombieSpeed = 0.4f;
	public float zombieTurnSpeed = 1f;
	public float zombieHearingRadius = 20f;
	public float zombieHuntingSpeedMultiplier = 2.5f;
	public float zombieIdleDestinationRadius = 30f;
	public float zombieAttackDamage = 5f;
	public float destinationReachedTreshold = 3f;

	//Health Default Configs
	public float startingHealth = 100f;

	//Spawner Defaut Configs
	public float spawnLoopTime = 15f;
}
