﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class SimpleZombieHealth : NetworkBehaviour {
	public float startingHealth;                  // The amount of health each zombie starts with.

	[SyncVar(hook = "OnCurrentHealthChanged")]
	private float currentHealth;                  // Zombie current health
	[SyncVar]
	private bool zombieHealthDepleated;           // true if zombie health reach <= 0

	private Animator animator;
	public RectTransform healthBar;

	void Start(){
		animator = gameObject.GetComponent<Animator>();
		currentHealth = startingHealth;
		zombieHealthDepleated = false;
	}

	public void SetConfigs(SimpleZombieConfigs configs){
		startingHealth = configs.startingHealth;
	}

	public override void OnStartClient(){
		OnCurrentHealthChanged(currentHealth);
	}
		
	void OnCurrentHealthChanged(float value){
		currentHealth = value;
		SetHealthIndicator();
	}

	private void SetHealthIndicator(){
		healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
	}

	public bool isZombieDead(){
		return (currentHealth <= 0f);
	}

	public void Damage(float amount, string attacker){
		if(zombieHealthDepleated)
			return;
		
		currentHealth -= amount;

		if (isServer && isZombieDead()) {
			zombieHealthDepleated = true;
			GameManager.s_Instance.AddKillScore(attacker);
			RpcKillZombie ();
		}
	}
		
	[ClientRpc]
	private void RpcKillZombie(){
		gameObject.GetComponent<NetworkIdentity>().localPlayerAuthority = true;
		animator.SetTrigger("Dead");
		gameObject.GetComponent<SimpleZombieController>().KillZombie();
	}
}
