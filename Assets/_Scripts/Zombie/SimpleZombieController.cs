﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SimpleZombieController : NetworkBehaviour {
	private CapsuleCollider zombieCollider;                      // Reverence to collider to disable on death
	private SimpleZombieMovement zombieMovementComponent;        // Zombie Components to set configs.    
	private SimpleZombieHealth zombieHealthComponent;

	public Canvas healthbar;                                     // Reference to healthbar to hide on death

	public void Awake(){
		zombieMovementComponent = gameObject.GetComponent<SimpleZombieMovement>();
		zombieHealthComponent = gameObject.GetComponent<SimpleZombieHealth>();
		zombieCollider = GetComponent<CapsuleCollider>();

		SetDefaultConfigs();
	}
		
	public void SetDefaultConfigs(){
		zombieMovementComponent.SetConfigs(new SimpleZombieConfigs());
		zombieHealthComponent.SetConfigs(new SimpleZombieConfigs());
	}

	public void KillZombie(){
		zombieMovementComponent.enabled = false;
		zombieCollider.enabled = false;
		healthbar.enabled = false;
		CmdUpdateGameOnKill();

		StartCoroutine(RemoveZombie());
	}

	[Command]
	public void CmdUpdateGameOnKill(){
		GameManager.s_Instance.RemoveZombie(gameObject);
	}

	IEnumerator RemoveZombie(){
		yield return new WaitForSeconds(3f);
		NetworkServer.Destroy(gameObject);
	}
}
