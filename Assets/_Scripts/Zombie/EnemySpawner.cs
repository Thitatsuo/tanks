﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;


public class EnemySpawner:NetworkBehaviour {

	public GameObject enemyPrefab;
	public float spawnLoopTime;      // Loop time of spawning enemies

	public override void OnStartServer(){
		SetConfigs(new SimpleZombieConfigs());
		StartCoroutine(SpawnLoop());
	}

	public void SetConfigs(SimpleZombieConfigs configs){
		spawnLoopTime = configs.spawnLoopTime;
	}

	public void SpawnEnemy(){
		var enemy = (GameObject)Instantiate(enemyPrefab, transform.position, transform.rotation);
		NetworkServer.Spawn(enemy);
		// Tells Game Manager that new enemy was created
		GameManager.s_Instance.AddZombie(enemy);
	}

	private IEnumerator SpawnLoop(){
		if(isServer){
			// wait for spawn time
			yield return new WaitForSeconds(spawnLoopTime);

			// if not game ended, then spawn enemy, otherwise stop routine
			if (!GameManager.s_Instance.gameIsFinished) {
				SpawnEnemy();
				StartCoroutine (SpawnLoop ());
			}
		}
	}
}
