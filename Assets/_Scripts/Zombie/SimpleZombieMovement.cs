﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;

public class SimpleZombieMovement : NetworkBehaviour {
    public float zombieSpeed;                        // Zombie idle speed
    public float zombieTurnSpeed;                    // Zombie idle turn speed
    public float zombieHearingRadius;                // Radius that zonbies can target a tank
    public float zombieHuntingSpeedMultiplier;       // Speed Multiplier when zombie following a tank
	public float zombieIdleDestinationRadius;        // Max Radius that a zombie can reach on esch iteration of navmesh
	public float zombieAttackDamage;                 // Amount of damage per second of a zombie
	private float destinationReachedTreshold;        // Minimum Radius to considerer that zonbie reached navmesh goal

    private Animator animator;
    private int playersLayerMask;
	private int playersLayerID;
	private bool idleMovement = false;
    public Transform tankTarget;
    private NavMeshAgent navMeshAgent;

	public AudioSource zombieAudio;
	public List<AudioClip> zombieIdleSounds; 
	public List<AudioClip> zombieAttackSounds; 

    void Awake(){
        animator = gameObject.GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent> ();
		playersLayerMask = LayerMask.GetMask("Players");
		playersLayerID = LayerMask.NameToLayer("Players");
    }

	public void SetConfigs(SimpleZombieConfigs configs){
		zombieSpeed = configs.zombieSpeed;
		zombieTurnSpeed = configs.zombieTurnSpeed;
		zombieHearingRadius = configs.zombieHearingRadius;  
		zombieHuntingSpeedMultiplier = configs.zombieHuntingSpeedMultiplier;
		zombieIdleDestinationRadius = configs.zombieIdleDestinationRadius;
		zombieAttackDamage = configs.zombieAttackDamage;
		destinationReachedTreshold = configs.destinationReachedTreshold;
	}
		
    void FixedUpdate(){
        if(!isServer)
            return;

		// check if has any tank whithin hearing radius to update target
        UpdateTarget();
        if(tankTarget)
        {
            StopIdleMovement();
            transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (tankTarget.transform.position - transform.position), zombieTurnSpeed * Time.deltaTime);
            transform.position += transform.forward * Time.deltaTime * zombieSpeed * zombieHuntingSpeedMultiplier;
            RpcUpdateMoveAnimation(zombieSpeed * zombieHuntingSpeedMultiplier);
        }
        else
        {
            IdleMovement();
        }
    }

    void UpdateTarget(){
		Collider[] colliders = Physics.OverlapSphere(transform.position, zombieHearingRadius, playersLayerMask);

        Transform nearestHearingTarget = null;
        float minDistance = Mathf.Infinity;
        for(int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
            SimpleTankController tankController = colliders[i].GetComponent<SimpleTankController>();
            bool tankDestroyed = tankController && tankController.isDestroyed();

            if(!targetRigidbody || tankDestroyed)
                continue;

            if(tankTarget == colliders[i].transform)
            {
                if(tankDestroyed)
                    tankTarget = null;
                return;
            }

            float distance = Vector3.Distance(colliders[i].transform.position, transform.position);

            if(distance < minDistance) {
                minDistance = distance;
                nearestHearingTarget = colliders[i].transform;
            }
        }

		if (tankTarget != nearestHearingTarget) 
		{
			zombieAudio.clip = zombieIdleSounds[Random.Range(0, zombieIdleSounds.Count)];
			zombieAudio.Play();
		}

		if (tankTarget != nearestHearingTarget) 
		{
			PlaySound("Idle");
		}

		tankTarget = nearestHearingTarget;
    }

	private void PlaySound(string soundType){
		if (soundType == "Attack") 
		{
			zombieAudio.clip = zombieAttackSounds[Random.Range (0, zombieAttackSounds.Count)];
		} 
		else if (soundType == "Idle") 
		{
			zombieAudio.clip = zombieIdleSounds[Random.Range (0, zombieIdleSounds.Count)];
		}

		zombieAudio.Play();
	}

    void IdleMovement(){
        if(!idleMovement)
        {
            idleMovement = true;
            navMeshAgent.isStopped = false;
            RpcUpdateMoveAnimation(zombieSpeed);
            navMeshAgent.SetDestination(RandomNavmeshLocation(zombieIdleDestinationRadius));
        }
        else
        {
            CheckIdleDestinationReached();
        }
    }

	// pick a random goal inside a max radius
    public Vector3 RandomNavmeshLocation(float radius){
        Vector3 randomDirection = Random.insideUnitSphere * Random.Range(destinationReachedTreshold, radius);
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;

        if(NavMesh.SamplePosition(randomDirection, out hit, radius, 1)){
            finalPosition = hit.position;
        }
        return finalPosition;
    }

    private void StopIdleMovement(){
        idleMovement = false;
        navMeshAgent.isStopped = true;
    }

    void CheckIdleDestinationReached(){
        float distanceToTarget = Vector3.Distance(transform.position, navMeshAgent.destination);
        if(distanceToTarget < destinationReachedTreshold)
            StopIdleMovement ();
    }

    [ClientRpc]
	private void RpcUpdateMoveAnimation(float zombieSpeed){
        animator.SetFloat("MoveSpeed", zombieSpeed);
    }

	[ClientRpc]
	private void RpcUpdateAttackAnimation(){
		animator.SetTrigger("Attack");
		PlaySound("Attack");
	}
		
	[ServerCallback]
	private void OnCollisionStay(Collision other){
		var collided = other.gameObject;
		if(collided.layer == playersLayerID && !collided.GetComponent<SimpleTankController>().isDestroyed()) 
		{
			RpcUpdateAttackAnimation();
			collided.GetComponent<TankHealth>().Damage(zombieAttackDamage * Time.deltaTime);
		}
	}
}
