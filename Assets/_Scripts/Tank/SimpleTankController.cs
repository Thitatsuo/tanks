﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class SimpleTankController : NetworkBehaviour {

	public Transform cameraPositionTransform;          // Tank's camera following position transform.

	private TankMovement tankMovementComponent;        // Tank Components to set configs and disable controls.
	private TankShooting tankShootingComponent;        
	private TankHealth tankHealthComponent;

	public GameObject tankRenderers;                   // Tank's Renderers to disable when inactive 
	public GameObject tankHealthCanvas;
	public GameObject tankLaunchForceCanvas;
	public GameObject tankLeftDustTrail;
	public GameObject tankRightDustTrail;
	public GameObject bustedTank;

	public Text nameText;

	private BoxCollider tankCollider;                 
	private bool tankDestroyed = false;                // This flag indicates if Tank was destroyed

	[Header("Network")]
	[Space]
	[SyncVar]
	public Color playerColor;
	[SyncVar]
	public string playerName;

	public void Awake(){
		tankMovementComponent = gameObject.GetComponent<TankMovement>();
		tankShootingComponent = gameObject.GetComponent<TankShooting>();
		tankHealthComponent = gameObject.GetComponent<TankHealth>();

		tankCollider = GetComponent<BoxCollider>();
		SetDefaultConfigs();
		SetControlEnabled(false);			
	}

	public override void OnStartClient()
	{
		base.OnStartClient();

		GameObject tankRenderers = transform.Find("TankRenderers").gameObject;
		Renderer[] renderers = tankRenderers.GetComponentsInChildren<Renderer>();
		for(int i = 0; i < renderers.Length; i++)
		{
			renderers[i].material.color = playerColor;
		}

		nameText.text = "<color=#" + ColorUtility.ToHtmlStringRGB(playerColor) + ">"+playerName+"</color>";

		if(tankHealthComponent.healthDepleated())
			DestroyTank();
	}

	public override void OnStartLocalPlayer(){
		EventManager.LocalPlayerCreated(gameObject);
	}

	public Transform GetCameraPositionTransform(){
		return cameraPositionTransform;
	}

	public void SetDefaultConfigs(){
		//		TankScritableObject simpleTankConfigs = Resources.Load<TankScritableObject>("Tanks/SimpleTankConfigs");
		//		tankShootingComponent.SetConfigs(simpleTankConfigs);
		//		tankMovementComponent.SetConfigs(simpleTankConfigs);
		//		tankHealthComponent.SetConfigs(simpleTankConfigs, DestroyTank);

		tankShootingComponent.SetConfigs(new SimpleTankConfigs());
		tankMovementComponent.SetConfigs(new SimpleTankConfigs());
		tankHealthComponent.SetConfigs(new SimpleTankConfigs(), DestroyTank);
	}

	public void SetControlEnabled(bool enable){
		tankMovementComponent.enabled = enable;
		tankShootingComponent.enabled = enable;
	}

	public void SetTankActive(bool active){
		tankCollider.enabled = active;

		tankRenderers.SetActive(active);
		tankHealthCanvas.SetActive(active);
		tankLaunchForceCanvas.SetActive(active);
		tankLeftDustTrail.SetActive(active);
		tankRightDustTrail.SetActive(active);

		SetControlEnabled(false);
	}

	public void DestroyTank(){
		tankDestroyed = true;
		SetTankActive(false);
		bustedTank.SetActive(true);
		tankCollider.enabled = true;
		tankMovementComponent.EngineAudio(false);
		GetComponent<Rigidbody>().isKinematic = true;
		CmdUpdateGameOnDestroy();
	}

	[Command]
	public void CmdUpdateGameOnDestroy(){
		GameManager.s_Instance.RemoveTank(gameObject);
	}

	public bool isDestroyed() {
		return tankDestroyed;
	}

	public void UpdateScore(int score){
		if(isLocalPlayer)
			GameManager.s_Instance.UpdateScoreText(playerName, score);
	}

	public string GetPlayerName(){
		return playerName;
	}
}
