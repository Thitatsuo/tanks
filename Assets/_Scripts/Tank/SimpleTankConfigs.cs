﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class SimpleTankConfigs {

	//shooting Default Configs
	public float minLaunchForce = 13f;
	public float maxLaunchForce = 20f;
	public float maxChargeTime = 5f;

	//moving Default Configs
	public float tankSpeed = 12f;
	public float tankTurnSpeed = 180f;
	public float tankEnginePitchVariation = 0.2f;
	public float runOverMaxDamage = 50f;
	public float runOverSelfDamage = 5f;

	//Health Default Configs
	public float startingHealth = 100f;
}
