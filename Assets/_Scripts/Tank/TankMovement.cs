﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TankMovement : NetworkBehaviour {

	public float tankSpeed;               // Tank's forward and back speed.
	public float tankTurnSpeed;           // Tank's turn Speed in degrees per second.
	public Rigidbody tankRigidbody;       // Tank's RigidBody reference.
	public float runOverMaxDamage;        // Tank's damage given when running over enemy.
	public float runOverSelfDamage;       // Tank's damage taken when running over enemy.

	private TankHealth tankHealthComponent;
	private float movementInput;                                // The current value of the movement input.
	private float turnInput;                                    // The current value of the turn input.
	private Vector3 currentVelocity = new Vector3(0, 0, 0);     // Current tank's velocity
	private int enemiesLayerID;

	public AudioSource movementAudio;                 // Reference to the audio source used to play engine sounds.
	public AudioClip tankEngineIdleClip;              // Audio clip of idle tank.
	public AudioClip tankEngineDrivingClip;           // Audio clip of moving tank.
	public float tankEnginePitchVariation;            // Variation of pitch of the engine noises.
	private float originalPitch;                      // The pitch of the audio source at the start of the scene.

	private SimpleTankController tankController;

	private void Awake(){
		tankRigidbody = GetComponent<Rigidbody>();
		tankHealthComponent = gameObject.GetComponent<TankHealth>();
		enemiesLayerID = LayerMask.NameToLayer("Enemies");
		tankController = gameObject.GetComponent<SimpleTankController>();
	}

	public void Start(){
		originalPitch = movementAudio.pitch;
	}

//	public void SetConfigs(TankScritableObject configs){
//		tankSpeed = configs.tankSpeed;
//		tankTurnSpeed = configs.tankTurnSpeed;
//		tankEnginePitchVariation = configs.tankEnginePitchVariation;   
//	}

	public void SetConfigs(SimpleTankConfigs configs){
		tankSpeed = configs.tankSpeed;
		tankTurnSpeed = configs.tankTurnSpeed;
		tankEnginePitchVariation = configs.tankEnginePitchVariation;  
		runOverMaxDamage = configs.runOverMaxDamage;
		runOverSelfDamage = configs.runOverSelfDamage;
	}

	private void Update(){
		if(!isLocalPlayer)
			return;

		movementInput = Input.GetAxis("Vertical1");
		turnInput = Input.GetAxis("Horizontal1");

		Move();
		Turn();
		EngineAudio(true);
	}

	private void Move(){
		currentVelocity = transform.forward * movementInput * tankSpeed;
		Vector3 movement = currentVelocity * Time.deltaTime;

		tankRigidbody.MovePosition(tankRigidbody.position + movement);
	}

	private void Turn(){
		float turn = turnInput * tankTurnSpeed * Time.deltaTime;
		Quaternion inputRotation = Quaternion.Euler(0f, turn, 0f);

		tankRigidbody.MoveRotation(tankRigidbody.rotation * inputRotation);
	}

	public Vector3 GetCurrentVelocity(){
		return currentVelocity;
	}

	public void EngineAudio(bool activate){
		if (!activate) 
		{
			movementAudio.Stop ();
			return;
		}

		if (Mathf.Abs(movementInput) < 0.1f && Mathf.Abs(turnInput) < 0.1f)
		{
			if (movementAudio.clip == tankEngineDrivingClip)
			{
				movementAudio.clip = tankEngineIdleClip;
				movementAudio.pitch = Random.Range(originalPitch - tankEnginePitchVariation, originalPitch + tankEnginePitchVariation);
				movementAudio.Play();
			}
		}
		else
		{
			if (movementAudio.clip == tankEngineIdleClip)
			{
				movementAudio.clip = tankEngineDrivingClip;
				movementAudio.pitch = Random.Range(originalPitch - tankEnginePitchVariation, originalPitch + tankEnginePitchVariation);
				movementAudio.Play();
			}
		}
	}
		
	private void OnCollisionStay(Collision other){
		var collided = other.gameObject;
		if (collided.layer == enemiesLayerID) 
		{
			float factor = Mathf.Abs(movementInput * Time.deltaTime);
			CmdApplyRunOverDamage(collided, factor * runOverMaxDamage, factor * runOverSelfDamage);
		}
	}

	[Command]
	public void CmdApplyRunOverDamage(GameObject zombie, float zombieDamage, float selfDamage){
		zombie.GetComponent<SimpleZombieHealth>().Damage(zombieDamage, tankController.GetPlayerName());
		tankHealthComponent.Damage(selfDamage);
	}
}

