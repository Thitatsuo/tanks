﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class TankShooting : NetworkBehaviour {

	public GameObject tankShell;               // Prefab of the shell.
	public Transform shellSpawnerTransform;    // Tank's child where shells are spawned.
	public Slider launchForceSlider;           // Tank's child that indicates shell launch force.
	public float minLaunchForce;               // Minimum force of shell fire.
	public float maxLaunchForce;               // Maximum force of shell fire.
	public float maxChargeTime;                // charge time to at max force.

	private string fireButton;                 // The input axis used launching shells.
	private float currentLaunchForce;          // The current force that will be applied to the shell when the fire button is released.
	private float chargeSpeed;                 // The speed the launch force increases.
	private bool flagFired;                    // Flag that indicates if a shell has been launched.

	public AudioSource tankShootingAudioSource;       // Reference to the audio source used to play the shooting audio.
	public AudioClip chargingClip;                    // Audio that plays when each shot is charging up.
	public AudioClip fireClip;                        // Audio that plays when each shot is fired.

	private void Start(){
		fireButton = "Fire1";
		currentLaunchForce = minLaunchForce;
	}

//	public void SetConfigs(TankScritableObject configs){
//		minLaunchForce = configs.minLaunchForce;
//		maxLaunchForce = configs.maxLaunchForce;
//		maxChargeTime = configs.maxChargeTime;   
//	}

	public void SetConfigs(SimpleTankConfigs configs){
		minLaunchForce = configs.minLaunchForce;
		maxLaunchForce = configs.maxLaunchForce;
		maxChargeTime = configs.maxChargeTime;   
	}

	private void Update(){
		if(!isLocalPlayer)
			return;

		// Update lauchForceSlider and calculate current charge speed
		launchForceSlider.value = GetlaunchForceSliderValue();
		chargeSpeed = (maxLaunchForce - minLaunchForce) / maxChargeTime;

		if (Input.GetButtonDown(fireButton))
		{
			// Reset the fired flag and reset the launch force.
			flagFired = false;
			currentLaunchForce = minLaunchForce;

			// Change the clip to the charging clip and start it playing.
			tankShootingAudioSource.clip = chargingClip;
			tankShootingAudioSource.PlayDelayed(0.5f);
		}

		// Otherwise, if the fire button is being held and the shell hasn't been launched yet
		else if (Input.GetButton(fireButton) && !flagFired)
		{
			if(currentLaunchForce >= maxLaunchForce) 
				currentLaunchForce = maxLaunchForce; 
			else
				currentLaunchForce += chargeSpeed * Time.deltaTime;

			launchForceSlider.value = GetlaunchForceSliderValue();
		}

		// Otherwise, if the fire button is released and the shell hasn't been launched yet
		else if(Input.GetButtonUp(fireButton) && !flagFired)
		{
			Fire();
		}
	}

	// Calculates and return lauch force value normalized
	private float GetlaunchForceSliderValue(){
		return (currentLaunchForce - minLaunchForce) * 100 / (maxLaunchForce - minLaunchForce);
	}

	//Fire the shell locally
	private void Fire(){
		// Set the fired flag so only Fire is only called once.
		flagFired = true;

		// Change the clip to the firing clip and play it.
		tankShootingAudioSource.clip = fireClip;
		tankShootingAudioSource.Play();

		TankMovement tankMovement = gameObject.GetComponent<TankMovement>();
		Vector3 tankVelocity = tankMovement.GetCurrentVelocity();

		SimpleTankController tankController = gameObject.GetComponent<SimpleTankController>();

		CmdFire(tankVelocity, currentLaunchForce, shellSpawnerTransform.forward, shellSpawnerTransform.position, shellSpawnerTransform.rotation, tankController.GetPlayerName());

		currentLaunchForce = minLaunchForce;
	}

	[Command]
	private void CmdFire(Vector3 tankVelocity, float launchForce, Vector3 forward, Vector3 position, Quaternion rotation, string owner){
		var shell = (GameObject)Instantiate (
			tankShell,
			position,
			rotation);

		shell.GetComponent<SimpleShellExplosion>().setOwner(owner);

		Vector3 velocity = tankVelocity + launchForce * forward;
		shell.GetComponent<Rigidbody>().velocity = velocity;

		NetworkServer.Spawn(shell);
	}
}
