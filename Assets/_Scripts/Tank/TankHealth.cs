﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class TankHealth : NetworkBehaviour {

	public delegate void OnHealthDepleated();
	private static event OnHealthDepleated _onHealthDepleated;

	public float startingHealth;                    // The amount of health each tank starts with.
	public Slider healthSlider;                     // The slider to represent how much health the tank currently has.
	public Image healthSliderFillImage;             // The image component of the slider.
	public Color fullHealthColor = Color.green;     // The color the health bar will be when on full health.
	public Color depleatedHealthColor = Color.red;  // The color the health bar will be when on no health.
	public ParticleSystem explosionParticles;       // The particle system the will play when the tank is destroyed.

	public AudioClip tankExplosionClip;             // The clip to play when the tank explodes.

	[SyncVar(hook = "OnCurrentHealthChanged")]
	private float currentHealth;                    // Tanks Current health.
	[SyncVar]
	private bool tankHealthDepleated;               // True if tans has no health.

	void Start(){
		currentHealth = startingHealth;
		tankHealthDepleated = false;
	}

	public override void OnStartClient(){
		OnCurrentHealthChanged(currentHealth);
	}

//	public void SetConfigs(TankScritableObject configs, OnHealthDepleated onHealthDepleated){
//	    startingHealth = configs.startingHealth;
//		_onHealthDepleated = onHealthDepleated;
//	}

	public void SetConfigs(SimpleTankConfigs configs, OnHealthDepleated onHealthDepleated){
		startingHealth = configs.startingHealth;
		_onHealthDepleated = onHealthDepleated;
	}

	public void Damage(float amount){
		if(tankHealthDepleated)
			return;

		currentHealth -= amount;

		if(currentHealth <= 0f) 
			KillTank();
	}

	void OnCurrentHealthChanged(float value){
		currentHealth = value;
		SetHealthIndicator();
	}

	private void SetHealthIndicator(){
		healthSlider.value = currentHealth;
		healthSliderFillImage.color = Color.Lerp(depleatedHealthColor, fullHealthColor, currentHealth / startingHealth);
	}

	public bool healthDepleated(){
		return tankHealthDepleated;
	}

	private void KillTank(){
		tankHealthDepleated = true;
		RpcKillTank();
	}

	[ClientRpc]
	private void RpcKillTank(){
		explosionParticles.Play();
		gameObject.GetComponent<SimpleTankController>().DestroyTank();
	}
}
