﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveInfo
{
	public List<PlayerScoreInfo> scoreInfos = new List<PlayerScoreInfo>();


	public PlayerScoreInfo getByIndex(int i){
		if (i < scoreInfos.Count) {
			return scoreInfos[i];
		} else {
			return null;
		}
	}

	public List<PlayerScoreInfo> GetSavedScoreInfos(){
		return scoreInfos;
	}

	public void RemoveAll(){
		scoreInfos = new List<PlayerScoreInfo>();
	}

	public void RemoveInfo(int infoIndex){
		scoreInfos.RemoveAt(infoIndex);
	}

	public void AddInfo(string name, int score){
		scoreInfos.Add(new PlayerScoreInfo(name, score));
	}
}
