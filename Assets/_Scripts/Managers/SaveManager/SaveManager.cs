﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
	public SaveInfo save;

	private static SaveManager _instance;
	public static SaveManager Instance{
		get { return _instance ?? (_instance = FindObjectOfType<SaveManager>()); }
	}

	private void OnDestroy(){
		if (_instance == this)
		{
			_instance = null;
		}
	}

	void Awake(){
		DontDestroyOnLoad(this.gameObject);
		LoadGame();
	}

	public void SaveGame(){
		var savedGame = JsonUtility.ToJson(save);
		PlayerPrefs.SetString("Save", savedGame);
		PlayerPrefs.Save();
	}

	public void LoadGame(){
		var loadedGame = PlayerPrefs.GetString("Save", string.Empty);
		save = JsonUtility.FromJson<SaveInfo>(loadedGame);
		if (save == null)
			save = new SaveInfo();
	}

	public void AddSave(string name, int score){
		save.AddInfo(name, score);
		SaveGame();
	}

	public void DeleteSave(int index){
		save.RemoveInfo(index);
		SaveGame();
	}

	public void DeleteAll(){
		save.RemoveAll();
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
	}
}
