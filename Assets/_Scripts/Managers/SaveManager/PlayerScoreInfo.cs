﻿using System;

[Serializable]
public class PlayerScoreInfo 
{
	public string playerName;
	public int score;

	public PlayerScoreInfo(string name, int playerScore){
		playerName = name;
		score = playerScore;
	}
}
