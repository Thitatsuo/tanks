﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EventManager : NetworkBehaviour {
	
	public delegate void OnLocalPlayerStarted(GameObject tank);
	public static event OnLocalPlayerStarted onLocalPlayerStarted;

	public static void LocalPlayerCreated(GameObject tank){
		if(onLocalPlayerStarted != null)
			onLocalPlayerStarted(tank);
	}
}
