﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;
using Prototype.NetworkLobby;

public class GameManager : NetworkBehaviour {
	static public GameManager s_Instance;

	//this is static so tank can be added even withotu the scene loaded 
	static public List<GameObject> tanksList = new List<GameObject>();   // A collection of tanks gameObject for enabling and disabling different aspects of the tanks.
	static public Dictionary<string, int> scores = new Dictionary<string, int>();

	public List<GameObject> zombiesList = new List<GameObject>();        // A collection of zombies gameObject for controlling status of round.

	public float startDelay = 3f;           // The delay between the start of RoundStarting and RoundPlaying phases.
	public float endDelay = 3f;             // The delay between the end of RoundPlaying and RoundEnding phases.
	public Text messageText;                // Reference to the overlay Text to display message text, etc.
	public Text playerScoreText;            // Reference to the overlay Text to display message text, etc.
	[HideInInspector]
	[SyncVar]
	public bool gameIsFinished = false;

	private WaitForSeconds startWait;         // Used to have a delay whilst the round starts.
	private WaitForSeconds endWait;           // Used to have a delay whilst the round or game ends.

	void Awake(){
		s_Instance = this;
	}

	[ServerCallback]
	private void Start(){
		// Create the delays so they only have to be made once.
		startWait = new WaitForSeconds(startDelay);
		endWait = new WaitForSeconds(endDelay);

		DisableTankControl();
		StartCoroutine(GameLoop());
	}

	// This is called from start and will run each phase of the game one after another. ONLY ON SERVER (as Start is only called on server)
	private IEnumerator GameLoop(){
		
		while (tanksList.Count < 2)
			yield return null;

		UpdateScores();

//		yield return new WaitForSeconds(2.0f);

		// Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
		yield return StartCoroutine(RoundStarting());

		// Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
		yield return StartCoroutine(RoundPlaying());

		// Once execution has returned here, run the 'RoundEnding' coroutine.
		yield return StartCoroutine(RoundEnding());

		SaveScores();

		float leftWaitTime = 15.0f;
		int flooredWaitTime = 15;
		while(leftWaitTime > 0.0f)
		{
			yield return null;
			leftWaitTime -= Time.deltaTime;

			int newFlooredWaitTime = Mathf.FloorToInt(leftWaitTime);

			if(newFlooredWaitTime != flooredWaitTime)
			{
				flooredWaitTime = newFlooredWaitTime;
				string message = EndMessage(Mathf.Max(0, flooredWaitTime));
				RpcUpdateMessage(message);
			}
		}

		ClearLists();
		LobbyManager.s_Singleton.ServerReturnToLobby();
	}

	// On Start Round, disable players control and show start message.
	private IEnumerator RoundStarting(){
		DisableTankControl();
		RpcRoundStarting();

		// Wait for the specified length of time until yielding control back to the game loop.
		yield return startWait;
	}

	[ClientRpc]
	void RpcRoundStarting(){
		messageText.text = "READY...";
		StartCoroutine(ClientRoundStartingFade());
	}
		
	private IEnumerator ClientRoundStartingFade(){
		yield return new WaitForSeconds(startDelay * 0.5f);
		messageText.CrossFadeAlpha (0, startDelay * 0.5f, false);
	}

	// On Playing Round, enable players control, show playing message and loop check gameStatus
	private IEnumerator RoundPlaying(){
		//notify clients that the round is now started, they should allow player to move.
		EnableTankControl();
		RpcRoundPlaying();

		while(!CheckEndGame()){
			yield return null;
		}

		gameIsFinished = true;
	}

	[ClientRpc]
	void RpcRoundPlaying(){
		messageText.CrossFadeAlpha (1f, 0f, false);
		messageText.text = "GO!!";

		StartCoroutine(ClientRoundPlayingFade());
	}

	private IEnumerator ClientRoundPlayingFade(){
		yield return new WaitForSeconds(0.5f);
		messageText.text = string.Empty;
	}

	// On Ending Round, disable players control and show end message.
	private IEnumerator RoundEnding(){
		DisableTankControl();
		RpcRoundEnding();

		// Wait for the specified length of time until yielding control back to the game loop.
		yield return endWait;
	}

	[ClientRpc]
	private void RpcRoundEnding(){
		messageText.text = "Game Over!!";
	}

	[ClientRpc]
	private void RpcUpdateMessage(string msg){
		messageText.text = msg;
	}
		
	// Game ends if ther is no Tanks or no zombies.
	private bool CheckEndGame(){
		return tanksList.Count < 1 || zombiesList.Count < 1;
	}

	// Save scores on playerPref
	private void SaveScores(){
		foreach (KeyValuePair<string, int> tmp in scores) {
			SaveManager.Instance.AddSave (tmp.Key, tmp.Value);
		}
	}
		
	// Returns a string of each player's score.
	private string EndMessage(int waitTime){
		string message = "<size=30 >";

		foreach (KeyValuePair<string, int> tmp in scores)
			message += tmp.Key + ": " + tmp.Value + " KILLS\n";

		message += "</size>\n<size=20 > Return to lobby in " + waitTime + "</size>\n";
		return message;
	}

	// Reset Tanks and Zombies Lists
	private void ClearLists(){
		tanksList = new List<GameObject>();
		zombiesList = new List<GameObject>();
		scores = new Dictionary<string, int>();
	}

	private void EnableTankControl(){
		for(int i = 0; i < tanksList.Count; i++)
		{
			RpcSetControlEnabled(tanksList[i], true);
		}
	}

	private void DisableTankControl(){
		for(int i = 0; i < tanksList.Count; i++)
		{
			RpcSetControlEnabled(tanksList[i], false);
		}
	}

	[ClientRpc]
	private void RpcSetControlEnabled(GameObject tank, bool active){
		tank.GetComponent<SimpleTankController>().SetControlEnabled(active);
	}
		
	static public void AddTank(GameObject tank, int playerNum, Color c, string name, int localID){
		SimpleTankController tankController = tank.GetComponent<SimpleTankController>();
		tankController.playerColor = c;
		tankController.playerName = name;

		tanksList.Add(tank);
		scores.Add(name, 0);
	}

	public void RemoveTank(GameObject tank){
		GameObject toRemove = null;
		foreach(var tmp in tanksList)
		{
			if(tmp == tank)
			{
				toRemove = tmp;
				break;
			}
		}
		if(toRemove != null)
			tanksList.Remove(toRemove);
	}

	public void AddZombie(GameObject zombie){
		zombiesList.Add(zombie);
	}

	public void RemoveZombie(GameObject zombie){
		GameObject toRemove = null;
		foreach(var tmp in zombiesList)
		{
			if(tmp == zombie)
			{
				toRemove = tmp;
				break;
			}
		}

		if(toRemove != null)
			zombiesList.Remove(toRemove);
	}

	public void UpdateScoreText(string playerName, int playerScore){
		playerScoreText.text = playerName + "'s kills: " + playerScore;
	}

	private void UpdateScores(){
		if (!isServer)
			return; 
		
		for(int i = 0; i < tanksList.Count; i++)
		{
			string playerName = tanksList[i].GetComponent<SimpleTankController>().GetPlayerName();
			RpcUpdateScore(tanksList[i], scores[playerName]);
		}
	}

	[ClientRpc]
	private void RpcUpdateScore(GameObject tank, int score){
		tank.GetComponent<SimpleTankController>().UpdateScore(score);
	}

	public void AddKillScore(string player){
		if(isServer){
			scores[player] += 1;
			UpdateScores();
		}
	}
}
