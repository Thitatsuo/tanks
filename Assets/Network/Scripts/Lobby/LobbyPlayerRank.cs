﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class LobbyPlayerRank : MonoBehaviour {
	public Text playerName;
	public Text playerScore;

	public void SetInfo(string name, int score){
		playerName.text = name;
		playerScore.text = score.ToString();
	}
}
