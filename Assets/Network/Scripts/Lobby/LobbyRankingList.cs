﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using Prototype.NetworkLobby;
using System.Linq;

public class LobbyRankingList : MonoBehaviour {
	public GameObject playerRankPrefab;
	public RectTransform playerListContentTransform;

	protected List<GameObject> playersRank = new List<GameObject>();

	public void Start(){
		UpdateRanking();
	}

	public void InsertRanking(){
		List<PlayerScoreInfo> rankingList = SaveManager.Instance.save.GetSavedScoreInfos();
		List<PlayerScoreInfo> savedEntriesSorted = rankingList.OrderByDescending(x => x.score).ToList();

		foreach(var tmp in savedEntriesSorted){
			GameObject playerInfo = Instantiate(playerRankPrefab);
			playerInfo.GetComponent<LobbyPlayerRank>().SetInfo(tmp.playerName, tmp.score);
			AddPlayer (playerInfo);
		}
	}

	public void AddPlayer(GameObject player)
	{
		playersRank.Add(player);
		player.transform.SetParent(playerListContentTransform, false);
	}

	public void RemoveAll(){
		foreach (Transform child in playerListContentTransform) {
			GameObject.Destroy(child.gameObject);
		}
	}
		
	public void UpdateRanking(){
		RemoveAll();
		InsertRanking();
	}

	public void DeleteAll(){
		RemoveAll();
		SaveManager.Instance.DeleteAll();
	}
}
